## To build a docker image:
docker build -t testspringimg -f deploy/Dockerfile .

## To run the image (e.g.):
docker run -itd --name testspringcontainer -p 172.31.8.70:9947:8082 -e DBNAME=northwind2 -e SERVERPORT=8082 -e DBHOST=mysql.conygre.com -e ELK=172.31.0.72 testspringimg

## To run bash in the image:
docker exec -it testspringcontainer /bin/bash

## To see running containers:
docker container ls

## To see all containers (including stopped):
docker container ls --all

## To see all images:
docker images

## To stop the container:
docker stop testspringcontainer

## To remove the container created above:
docker rm testspringcontainer

## To remove the image created above:
docker rmi testspringimg
